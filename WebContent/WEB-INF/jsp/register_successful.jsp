<%@page session="true"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Battleships | Success!</title>
</head>
<body>
	<%@include file="header.jsp" %>

	<h2>"You have registered successfully! You can now proceed to log in."!</h2>
	<p><a href="login.html">LOG IN</a></p>
</body>
</html>