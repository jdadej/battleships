<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8" %>

${status}

<c:if test="${status}">
	<script>
		self.parent.location.href = "wait2.html";
	</script>
</c:if>
<c:if test="${!status}">
	<script>
		window.location.href = "whomoves2.html";
	</script>
</c:if>

