<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/pokoje.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Battleships | Mulitplayer</title>
</head>
<body id="body1" background="images/bg3.png">
<div id="gora">
<a href="index.jsp"><img src="images/menubutton.png"/></a>
</div>
<div id="przewijanie" class="customfont">

	<p class="custromfont">Wybierz pokój</p>

	<c:forEach var="room" items="${rooms}" varStatus="roomIndex">

    	<p class="custromfont">${roomIndex.index} - <a href="room.html?room=${room.name}">${room.name}</a> (${room.login1},${room.login2})</p>

	</c:forEach>
</div>
</body>
</html>