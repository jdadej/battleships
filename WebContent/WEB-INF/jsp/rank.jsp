<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/ranking.css">
<title>Ranking</title>
</head>
<body id="body1" background="images/bg2.jpg">
<div id="gora">
<a href="index.jsp"><img src="images/menubutton.png"/></a>
</div>
<div id="left"></div>
<div>
	<div style= "float: left; ">
	<p id="credit1" class="customfont">
		<c:forEach var="nick" items="${ranking.nicks}" >
			&nbsp;&nbsp; ${nick}<br/>
		</c:forEach>
	</p>
	</div>
	<div style= "float: left; ">
	<p id="credit2" class="customfont">
		<c:forEach var="win" items="${ranking.wins}" >
			&nbsp;&nbsp; ${win}<br/>
		</c:forEach> 
	</p>
	</div>
</div>
</body>
</html>