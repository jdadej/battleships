<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/plansze.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <title>Battleships | Mulitplayer 2</title>
</head>
<body id="body1" background="images/bg3.png">
<div id="gora">
<a href="index.jsp"><img src="images/menubutton.png"/></a>
</div>
<br/><br/>
	<h2>Wybrany pokój: ${room.name}</h2>
	<h3>${room.login2} kontra ${room.login1}</h3>
<div id="plansza2">
<br/>
<c:forEach var="row" items="${game.player2.states}" varStatus="rowIndex">
	<c:forEach var="element" items="${row}" varStatus="columnIndex">
		<FORM method='post' action='multiPlaceShip2.html' style= "display: inline; ">
			<c:if test="${element==0}">
				<INPUT TYPE="submit" Name="placeShip" Value="${element}" style="height:50px; width:50px; background-image:url(images/woda3.jpg); font-size:0px;">
			</c:if>
			<c:if test="${element==1}">
				<INPUT TYPE="button" Name="placeShip" Value="${element}" style="height:50px; width:50px; background-image:url(images/wodax.jpg); font-size:0px;">
			</c:if>
			<c:if test="${element==2}">
				<INPUT TYPE="button" Name="placeShip" Value="${element}" style="height:50px; width:50px; background-color:green; font-size:0px;">
			</c:if>
			<c:if test="${element==3}">
				<INPUT TYPE="button" Name="placeShip" Value="${element}" style="height:50px; width:50px; background-image:url(images/wodax.jpg); font-size:0px;">
			</c:if>
			<INPUT TYPE="hidden" Name="row" Value="${rowIndex.index}">
			<INPUT TYPE="hidden" Name="column" Value="${columnIndex.index}">
		</FORM>
	</c:forEach>
	<br/>  
</c:forEach>    
</div>	
</body>
</html>