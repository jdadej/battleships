<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" href="css/plansze.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<title>Plansza</title>
</head>
<body id="body1" background="images/bg3.png">
<div id="gora">
<a href="index.jsp"><img src="images/menubutton.png"/></a>
</div>
<h2>Wybrany pok�j: ${room.name}</h2>
<h3>${room.login1} kontra ${room.login2}</h3>
<div>
<div id="plansza2">
<br/>
<br/>
<p>YOU</p>
<FORM method='post' action='winner.html'>
	<c:if test="${game.player1.winCounter==0}">
	<input type="submit" class="btn btn-default btn-lg" value="You WIN!!! Click to get some points to ranking"/>
	</c:if>
	<c:if test="${game.player2.winCounter==0}">
	<input type="button" class="btn btn-default btn-lg" value="You lose. Click to leave"   onClick="location.href='index.jsp'"/>
	</c:if>
</FORM>
<br/>
<c:forEach var="row" items="${game.player2.states}" varStatus="rowIndex">
	<c:forEach var="element" items="${row}" varStatus="columnIndex">
		<FORM method='post' action='multiMove.html' style= "display: inline; ">
			<c:if test="${element==0}">
				<c:if test="${game.whoMoves==1}">
					<INPUT TYPE="submit" Name="multiMove" Value="${element}" style="height:50px; width:50px; background-image:url(images/woda3.jpg); font-size:0px;">
				</c:if>
				<c:if test="${game.whoMoves==2}">
					<INPUT TYPE="button" Name="multiMove" Value="${element}" style="height:50px; width:50px; background-image:url(images/woda3.jpg); font-size:0px;">
				</c:if>
				<c:if test="${game.whoMoves==3}">
					<INPUT TYPE="button" Name="multiMove" Value="${element}" style="height:50px; width:50px; background-image:url(images/woda3.jpg); font-size:0px;">
				</c:if>
			</c:if>
			<c:if test="${element==1}">
				<INPUT TYPE="button" Name="multiMove" Value="${element}" style="height:50px; width:50px; background-image:url(images/wodax.jpg); font-size:0px;">
			</c:if>
			<c:if test="${element==2}">
				<INPUT TYPE="button" Name="multiMove" Value="${element}" style="height:50px; width:50px; background-image:url(images/shot.jpg); font-size:0px;">
			</c:if>
			<c:if test="${element==3}">
				<INPUT TYPE="button" Name="multiMove" Value="${element}" style="height:50px; width:50px; background-image:url(images/destroyed.jpg); font-size:0px;">
			</c:if>
			<INPUT TYPE="hidden" Name="row" Value="${rowIndex.index}">
			<INPUT TYPE="hidden" Name="column" Value="${columnIndex.index}">
		</FORM>
	</c:forEach>
	<br/>  
</c:forEach>    
</div>
<div id="plansza1">
<br/>
<br/>
<p>OPPONENT</p>
<br/>
<c:forEach var="row" items="${game.player1.states}" varStatus="rowIndex">
	<c:forEach var="element" items="${row}" varStatus="columnIndex">
		<FORM method='post' action='multiMove.html' style= "display: inline; ">
			<c:if test="${element==0}">
				<INPUT TYPE="button" Name="multiMove" Value="${element}" style="height:50px; width:50px; background-image:url(images/woda3.jpg); font-size:0px;">
			</c:if>
			<c:if test="${element==1}">
				<INPUT TYPE="button" Name="multiMove" Value="${element}" style="height:50px; width:50px; background-image:url(images/wodax.jpg); font-size:0px;">
			</c:if>
			<c:if test="${element==2}">
				<INPUT TYPE="button" Name="multiMove" Value="${element}" style="height:50px; width:50px; background-image:url(images/shot.jpg); font-size:0px;">
			</c:if>
			<c:if test="${element==3}">
				<INPUT TYPE="button" Name="multiMove" Value="${element}" style="height:50px; width:50px; background-image:url(images/destroyed.jpg); font-size:0px;">
			</c:if>
			<INPUT TYPE="hidden" Name="row" Value="${rowIndex.index}">
			<INPUT TYPE="hidden" Name="column" Value="${columnIndex.index}">
		</FORM>
	</c:forEach>
	<br/>  
</c:forEach>  
</div>
</div>
<c:if test="${game.whoMoves==2}">
<iframe src="whomoves1.html" style="display:none">
</iframe>
</c:if>   
</body>
</html>