<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">	
	<link rel="stylesheet" href="css/plansze.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<title>Ustaw Statki</title>
</head>
<body id="body1" background="images/bg3.png">
<div id="gora">
<a href="index.jsp"><img src="images/menubutton.png"/></a>
</div>
<div>
<div id="plansza2">
<br/>
<br/>
<p class="customfont">You</p>
<br/><br/>
<c:forEach var="row" items="${game.player1.states}" varStatus="rowIndex">
	<c:forEach var="element" items="${row}" varStatus="columnIndex">
		<FORM method='post' action='placeShip.html' style= "display: inline; ">
			<c:if test="${element==0}">
				<INPUT TYPE="submit" Name="placeShip" Value="${element}" style="height:50px; width:50px; background-image:url(images/woda3.jpg); font-size:0px;">
			</c:if>
			<c:if test="${element==1}">
				<INPUT TYPE="button" Name="placeShip" Value="${element}" style="height:50px; width:50px; background-image:url(images/wodax.jpg); font-size:0px;">
			</c:if>
			<c:if test="${element==2}">
				<INPUT TYPE="button" Name="placeShip" Value="${element}" style="height:50px; width:50px; background-color:green; font-size:0px;">
			</c:if>
			<c:if test="${element==3}">
				<INPUT TYPE="button" Name="placeShip" Value="${element}" style="height:50px; width:50px; background-image:url(images/wodax.jpg); font-size:0px;">
			</c:if>
			<INPUT TYPE="hidden" Name="row" Value="${rowIndex.index}">
			<INPUT TYPE="hidden" Name="column" Value="${columnIndex.index}">
		</FORM>
	</c:forEach>
	<br/>  
</c:forEach>    
</div>
</div>
</body>
</html>