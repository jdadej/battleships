<%@page session="true"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Battleships | Błąd</title>
</head>
<body>
	<%@include file="header.jsp" %>

	<h2>Ups, coś poszło nie tak!</h2>

	<h3>${message}</h3>
</body>
</html>