package pl.e3.ships;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.e3.ships.model.Battleship;
import pl.e3.ships.model.Game;
import pl.e3.ships.model.RankingList;
import pl.e3.ships.model.Room;
import pl.e3.ships.model.User;
import pl.e3.ships.model.UserDAO;
import pk.projekt.e2.sound.contract.IAudioManager;
import pk.projekt.e2.sound.impl.AudioManagerImpl;
import BazaDanych.*;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

@Controller
public class GameController {

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public ModelAndView createNewGame(HttpServletRequest request) {

		Game game = new Game();
		game.initiateAI(0, 0, 0);

		request.getSession().setAttribute("game", game);
		return new ModelAndView("place", "game", game);
	}
	
	@RequestMapping(value = "/continue", method = RequestMethod.GET)
	public ModelAndView continueGame(HttpServletRequest request) {

		Game game = (Game) request.getSession().getAttribute("game");
		request.getSession().setAttribute("game", game);
		if(game==null){	
			game = new Game();
			game.initiateAI(0, 0, 0);

			request.getSession().setAttribute("game", game);
			return new ModelAndView("place", "game", game);
		}else if(game.player1.counter==20){return new ModelAndView("plansza", "game", game);}
		else return new ModelAndView("place", "game", game);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/credits", method = RequestMethod.GET)
	public ModelAndView credits() {
		
		//przyklad wykorzystania komponentu zewnetrznego DB
        DB baza = DB.getDB();
        baza.CzyscBaze();
        List<String> tworcy = new ArrayList<String>();
		Map<Class, ArrayList> test =  new HashMap<Class, ArrayList>();;
        tworcy.add("Jakub Dadej");
        tworcy.add("Sabina Korpetta");
        tworcy.add("Maciej Tymek");
        baza.DodajObiekt(tworcy);
        baza.Zamknij();
        try {
	        FileInputStream fileIn = new FileInputStream("./baza/db.ser");
			@SuppressWarnings("resource")
			ObjectInputStream in = new ObjectInputStream(fileIn);
	        test = (Map<Class, ArrayList>) in.readObject();
	    } catch (Exception e) {
	        System.out.println(e);
	    }
        System.out.println(test);
		return new ModelAndView("credits");
	}
	
	@RequestMapping(value = "/waiting", method = RequestMethod.GET)
	public ModelAndView waiting4Game(HttpServletRequest request, @RequestParam("room") String room) {
		
		Battleship battleship = (Battleship)request.getServletContext().getAttribute("Battleship");
		Room currentRoom = battleship.findRoom(room);

		if( currentRoom != null ) {
			if( currentRoom.getLogin1()!=null && currentRoom.getLogin2()==null){
				// room1 ready
				request.getSession().setAttribute("room",currentRoom);
				request.getSession().setAttribute("game",currentRoom.getGame());
				return new ModelAndView("multigame1", "game", currentRoom.getGame());
			}
			else if( currentRoom.getLogin1()!=null && currentRoom.getLogin2()!=null){
				// room2 ready
				request.getSession().setAttribute("room",currentRoom);
				request.getSession().setAttribute("game",currentRoom.getGame());
				return new ModelAndView("multigame2", "game", currentRoom.getGame());
			}			
			else {
				// waiting
				return new ModelAndView("wybranypokoj", "room", currentRoom);
			}
		}

		return new ModelAndView("pokoje", "rooms", battleship.getAllRooms());
	}
	
	@RequestMapping(value = "/multi", method = RequestMethod.GET)
	public ModelAndView multiGame(HttpServletRequest request) {

		Battleship battleship = (Battleship)request.getServletContext().getAttribute("Battleship");
		if( battleship==null) {
			battleship = new Battleship();
			request.getServletContext().setAttribute("Battleship", battleship);
		}

		return new ModelAndView("pokoje", "rooms", battleship.getAllRooms());
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/room", method = RequestMethod.GET)
	public ModelAndView multiGame(HttpServletRequest request, @RequestParam("room") String room) {

		Battleship battleship = (Battleship)request.getServletContext().getAttribute("Battleship");
		Room currentRoom = battleship.findRoom(room);
		currentRoom.game.roomName=room;
		
		String login;
		Principal principal = request.getUserPrincipal();
		if( principal!=null ) {
			login = principal.getName();
		}
		else {
			login = "jonhy b";}

		if( currentRoom!=null) {
			if( currentRoom.getLogin1()==null){
				currentRoom.setLogin1(login);
				return new ModelAndView("wybranypokoj", "room", currentRoom);
			}
			else if( currentRoom.getLogin2()==null){
				currentRoom.setLogin2(login);
				return new ModelAndView("wybranypokoj", "room", currentRoom);
			}
			else {return new ModelAndView("error", "message", "Pok�j zaj�ty");}
		}
		return new ModelAndView("pokoje", "rooms", battleship.getAllRooms());
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	@RequestMapping(value = "/winner", method = RequestMethod.POST)
	public ModelAndView winner(HttpServletRequest request) {
		
		Game game = (Game) request.getSession().getAttribute("game");
		Battleship battleship = (Battleship)request.getServletContext().getAttribute("Battleship");
		Room currentRoom = battleship.findRoom(game.roomName);
		
		if(game.whoMoves==1){game.whoMoves=2;}else game.whoMoves=1;
		
		String winner=null;	
		Integer wins=0;
		if(game.player1.winCounter==0){winner=currentRoom.getLogin1();}
		else if(game.player2.winCounter==0){winner=currentRoom.getLogin2();}
		currentRoom.setLogin1(null);
		currentRoom.setLogin2(null);
		currentRoom.game=new Game();

		request.getSession().setAttribute("room",currentRoom);
		request.getSession().setAttribute("game",currentRoom.getGame());
		request.getServletContext().setAttribute("Battleship", battleship);
		
		Multimap<String, Integer> slownik = HashMultimap.create();
		    try {
		        FileInputStream fileIn = new FileInputStream("ranking.ser");
				@SuppressWarnings("resource")
				ObjectInputStream in = new ObjectInputStream(fileIn);
		        slownik = (Multimap) in.readObject();		        
		    } catch (Exception e) {
		        System.out.println(e);
		    }
		    if(slownik.containsKey(winner)){
		    wins=(Integer) slownik.get(winner).toArray()[0];
		    }
	        slownik.removeAll(winner);
	        slownik.put(winner, wins);
	        try {
	            FileOutputStream fileOut = new FileOutputStream("ranking.ser");
	            @SuppressWarnings("resource")
				ObjectOutputStream out = new ObjectOutputStream(fileOut);
	            out.writeObject(slownik);
	        } catch (Exception e) {
	            System.out.println(e);
	        }
		return new ModelAndView("return");
	}
   
	@RequestMapping(value = "/ranking", method = RequestMethod.GET)
	public ModelAndView ranking() {
		
		//przyklad wykorzystania komponentu zewnetrznego, ktory odtwarza dzwiek przy zaladowaniu rankingu
		IAudioManager audioManager = new AudioManagerImpl();
		audioManager.addSound("baseball", "./audio/baseball_hit.wav");
		try {
			audioManager.playSound("baseball");
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		RankingList ranking = new RankingList();
		 
		return new ModelAndView("rank", "ranking", ranking);
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}
		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView register(HttpServletRequest request, @RequestParam String nick, @RequestParam String haslo, @RequestParam String matchingpassword){
		
		ModelAndView model = new ModelAndView();
		List<User> users = UserDAO.listStudents();
		for (User record : users) {
			if (record.getUsername().equals(nick)){
				model.addObject("error", "This user already exists");
				model.setViewName("login");
				return model;
				}
		}			
		if (matchingpassword.equals(haslo))
			{					
				new UserDAO().createNewUser(nick, haslo);
				return new ModelAndView("register_successful");
			}
			else
			{
				model.addObject("error", "Passwords don't match!");
				model.setViewName("login");					
			}								
		return model;
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();
		// check if user is login
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("username", userDetail.getUsername());
		}
		model.setViewName("403");
		return model;
	}
	
	@RequestMapping(value = "/placeShip", method = RequestMethod.POST)
	public ModelAndView placeShip(HttpServletRequest request,
			@RequestParam String placeShip, @RequestParam String row,
			@RequestParam String column) {

		Game game = (Game) request.getSession().getAttribute("game");

		int r = Integer.parseInt(row);
		int c = Integer.parseInt(column);

		game.place1(r, c);
		request.getSession().setAttribute("game", game);

		if (game.player1.counter == 20) {
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					game.player1.states[x][y] = 0;
				}
			}
			return new ModelAndView("plansza", "game", game);
		} else
			return new ModelAndView("place", "game", game);
	}
	
	@RequestMapping(value = "/multiPlaceShip1", method = RequestMethod.POST)
	public ModelAndView multiPlaceShip1(HttpServletRequest request,
			@RequestParam String placeShip, @RequestParam String row,
			@RequestParam String column) {

		Game game = (Game) request.getSession().getAttribute("game");

		int r = Integer.parseInt(row);
		int c = Integer.parseInt(column);

		game.place1(r, c);
		request.getSession().setAttribute("game", game);

		if (game.player1.counter == 20) {
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					game.player1.states[x][y] = 0;
				}
			}
			return new ModelAndView("multiPlansza1", "game", game);
		} else
			return new ModelAndView("multigame1", "game", game);
	}
	@RequestMapping(value = "/multiPlaceShip2", method = RequestMethod.POST)
	public ModelAndView multiPlaceShip2(HttpServletRequest request,
			@RequestParam String placeShip, @RequestParam String row,
			@RequestParam String column) {

		Game game = (Game) request.getSession().getAttribute("game");

		int r = Integer.parseInt(row);
		int c = Integer.parseInt(column);

		game.place2(r, c);
		request.getSession().setAttribute("game", game);

		if (game.player2.counter == 20) {
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					game.player2.states[x][y] = 0;
				}
			}
			return new ModelAndView("multiPlansza2", "game", game);
		} else
			return new ModelAndView("multigame2", "game", game);
	}
	
	@RequestMapping(value = "/move", method = RequestMethod.POST)
	public ModelAndView makeMove(HttpServletRequest request,
			@RequestParam String move, @RequestParam String row,
			@RequestParam String column) {

		Game game = (Game) request.getSession().getAttribute("game");
		
		int r = Integer.parseInt(row);
		int c = Integer.parseInt(column);

		game.player1Move(r, c);
		if (game.shipsList2[r][c] == 1) {
			game.player2.states[r][c] = 3;
		}
		game.player1Checker();
		game.makeRed2();
		if(game.player1.correctShoot&&game.player1.winCounter>0){
			request.getSession().setAttribute("game", game);
			return new ModelAndView("plansza", "game", game);}
		
		if(game.player1.winCounter>0){game.AI();}

		request.getSession().setAttribute("game", game);
		if (game.player1.winCounter == 0 || game.player2.winCounter == 0) {
			return new ModelAndView("winner", "game", game);
		} else
			return new ModelAndView("plansza", "game", game);
	}
	
	@RequestMapping(value = "/multiMove", method = RequestMethod.POST)
	public ModelAndView multiMakeMove(HttpServletRequest request,
			@RequestParam String multiMove, @RequestParam String row,
			@RequestParam String column) {

		Game game = (Game) request.getSession().getAttribute("game");

		int r = Integer.parseInt(row);
		int c = Integer.parseInt(column);

		if(game.whoMoves==1){
			game.player1Move(r, c);
			if (game.shipsList2[r][c] == 1) {
				game.player2.states[r][c] = 3;
			}
			game.player1Checker();
			game.makeRed2();
			if(game.player1.correctShoot){				
					if(game.whoMoves==1){
						if(game.player1.winCounter==0){game.whoMoves=2;}
						request.getSession().setAttribute("game", game);
						return new ModelAndView("multiPlansza1", "game", game);
					}else {request.getSession().setAttribute("game", game);return new ModelAndView("multiPlansza1", "game", game);}				
					
			}
		}
		else if(game.whoMoves==2){
			game.player2Move(r, c);
			if (game.shipsList1[r][c] == 1) {
				game.player1.states[r][c] = 3;
			}
			game.player2Checker();
			game.makeRed1();
			if(game.player2.correctShoot){
					if(game.whoMoves==2){
						if(game.player2.winCounter==0){game.whoMoves=1;}
						request.getSession().setAttribute("game", game);
						return new ModelAndView("multiPlansza2", "game", game);
					}else {request.getSession().setAttribute("game", game);return new ModelAndView("multiPlansza2", "game", game);}					
			}
		}
		
		if((!game.player2.correctShoot)&&game.whoMoves==2){game.whoMoves=1;}else game.whoMoves=2;
		
		request.getSession().setAttribute("game", game);

			if(game.whoMoves==2){
				return new ModelAndView("multiPlansza1", "game", game);
			}else return new ModelAndView("multiPlansza2", "game", game);			
	}
	
	@RequestMapping(value = "/whomoves1", method = RequestMethod.GET)
	public ModelAndView whomoves1(HttpServletRequest request) {
		
		Game game = (Game) request.getSession().getAttribute("game");		
		/*if (game.player1.winCounter == 0 || game.player2.winCounter == 0) {
			//if(game.player1.winCounter == 0){game.whoMoves=2;}else game.whoMoves=1;
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					if(game.player2.states[x][y] == 0){
						if(game.shipsList2[x][y]==0){
							game.player2.states[x][y] = 1;
						}else game.player2.states[x][y] = 2;}
					}
				}			
			request.getSession().setAttribute("game", game);
			return new ModelAndView("multiplansza1", "game", game);
		}	*/	
		boolean status = game.whoMoves == 1;
		if (game.player1.winCounter == 0 || game.player2.winCounter == 0){
			game.whoMoves = 3;
			request.getSession().setAttribute("game", game);
			status=true;
			return new ModelAndView("whomoves1", "status", status);
			}
		return new ModelAndView("whomoves1", "status", status);
	}
	@RequestMapping(value = "/whomoves2", method = RequestMethod.GET)
	public ModelAndView whomoves2(HttpServletRequest request) {
		
		Game game = (Game) request.getSession().getAttribute("game");
		/*if (game.player1.winCounter == 0 || game.player2.winCounter == 0) {
			//if(game.player1.winCounter == 0){game.whoMoves=2;}else game.whoMoves=1;
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					if(game.player1.states[x][y] == 0){
						if(game.shipsList1[x][y]==0){
							game.player1.states[x][y] = 1;
						}else game.player1.states[x][y] = 2;}
					}
				}			
			request.getSession().setAttribute("game", game);
			return new ModelAndView("multiplansza2", "game", game);
		}	*/
		boolean status = game.whoMoves == 2;
		if (game.player1.winCounter == 0 || game.player2.winCounter == 0){
			game.whoMoves = 3;
			request.getSession().setAttribute("game", game);
			status=true;
			return new ModelAndView("whomoves2", "status", status);
			}
		return new ModelAndView("whomoves2", "status", status);
	}
	
	@RequestMapping(value = "/wait1")
	public ModelAndView wait1(HttpServletRequest request)
	{
		Game game = (Game) request.getSession().getAttribute("game");
		request.getSession().setAttribute("game", game);
		return new ModelAndView("multiPlansza1", "game", game);
	}
	@RequestMapping(value = "/wait2")
	public ModelAndView wait2(HttpServletRequest request)
	{
		Game game = (Game) request.getSession().getAttribute("game");
		request.getSession().setAttribute("game", game);
		return new ModelAndView("multiPlansza2", "game", game);
	}

}
