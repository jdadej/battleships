package pl.e3.ships.interfaces;

import pl.e3.ships.model.Game;

public interface IRoom {
	
	public String getName();

	public void setName(String name);

	public Game getGame();

	public void setGame(Game game);

	public String getLogin1();

	public void setLogin1(String login1);

	public String getLogin2();

	public void setLogin2(String login2);
}
