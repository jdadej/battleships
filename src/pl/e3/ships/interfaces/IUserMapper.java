package pl.e3.ships.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;

import pl.e3.ships.model.User;

public interface IUserMapper {
	public User mapRow(ResultSet rs, int rowNum) throws SQLException;
}
