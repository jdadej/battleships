package pl.e3.ships.interfaces;

import javax.sql.DataSource;


public interface IUserDAO {

	public void setDataSource(DataSource ds);
	public void createNewUser(String log, String pas);
}
