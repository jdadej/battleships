package pl.e3.ships.interfaces;

public interface IBoard {

	public int[][] getStates();
	public void setStates(int[][] states);
	public int getWinCounter();
	public void setWinCounter(int counter);
	public int getCounter();
	public void setCounter(int counter);
	
}
