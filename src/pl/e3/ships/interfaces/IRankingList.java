package pl.e3.ships.interfaces;


public interface IRankingList {

	public String[] getNicks();
	public void setNicks(String[] nicks);
	public String[] getWins();
	public void setWins(String[] wins);

}
