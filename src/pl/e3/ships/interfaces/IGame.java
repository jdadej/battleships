package pl.e3.ships.interfaces;

import pl.e3.ships.model.Board;

public interface IGame {
	

	public Board getPlayer1();

	public Board getPlayer2();

	public void setPlayer1(Board player1);

	public void setPlayer2(Board player2);

	public int getWhoMoves();

	public void setWhoMoves(int whoMoves);

	public int[][] getShipsList1();

	public void setShipsList1(int[][] shipslist1);

	public void initiateAI(int X, int Y, int zliczacz);
	
	Boolean checker(int x, int y);

	void makeFieldRed();

	public void place1(int X, int Y);

	public void place2(int X, int Y);

	public int player1Move(int r, int c);

	public int player2Move(int r, int c);

	public void player1Checker();

	public void player2Checker();

	public int AI();

	public void AIchecker();

	void makeRedAI(int sink);

	public void makeRed1();

	public void makeRed2();

}
