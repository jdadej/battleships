package pl.e3.ships.interfaces;

import java.util.List;

import pl.e3.ships.model.Room;

public interface IBattleship {
	
	public List<Room> getAllRooms();

	public Room findRoom(String roomName);
}
