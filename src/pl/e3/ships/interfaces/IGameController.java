package pl.e3.ships.interfaces;


import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

public interface IGameController<HttpServletRequest> {

	public ModelAndView createNewGame(HttpServletRequest request);
	
	public ModelAndView continueGame(HttpServletRequest request);
	
	public ModelAndView waiting4Game(HttpServletRequest request, @RequestParam("room") String room);
	
	public ModelAndView multiGame(HttpServletRequest request);


	public ModelAndView multiGame(HttpServletRequest request, @RequestParam("room") String room);

	public ModelAndView ranking();

	public ModelAndView login(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout);
	

	public ModelAndView register(HttpServletRequest request, @RequestParam String nick, @RequestParam String haslo, @RequestParam String matchingpassword);

	public ModelAndView accesssDenied();
	
	public ModelAndView placeShip(HttpServletRequest request,
			@RequestParam String placeShip, @RequestParam String row,
			@RequestParam String column);
	

	public ModelAndView multiPlaceShip1(HttpServletRequest request,
			@RequestParam String placeShip, @RequestParam String row,
			@RequestParam String column);
	
	public ModelAndView multiPlaceShip2(HttpServletRequest request,
			@RequestParam String placeShip, @RequestParam String row,
			@RequestParam String column);
	

	public ModelAndView makeMove(HttpServletRequest request,
			@RequestParam String move, @RequestParam String row,
			@RequestParam String column);
	

	public ModelAndView multiMakeMove(HttpServletRequest request,
			@RequestParam String multiMove, @RequestParam String row,
			@RequestParam String column);
	

	public ModelAndView whomoves1(HttpServletRequest request);
	

	public ModelAndView whomoves2(HttpServletRequest request);
	
	public ModelAndView wait1(HttpServletRequest request);
	
	
	public ModelAndView wait2(HttpServletRequest request);

}
