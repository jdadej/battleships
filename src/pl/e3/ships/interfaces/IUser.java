package pl.e3.ships.interfaces;

public interface IUser {

	public String getUsername();
	public void setUsername(String user);
	public String getPassword();
	public void setPassword(String pass);
}
