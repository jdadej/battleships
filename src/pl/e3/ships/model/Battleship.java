package pl.e3.ships.model;

import java.util.ArrayList;
import java.util.List;

import pl.e3.ships.interfaces.IBattleship;

public class Battleship implements IBattleship {
	
	List<Room> rooms = null;
	
	public List<Room> getAllRooms() {
		if (rooms == null) {
			rooms = new ArrayList<Room>();
			for(int i=1;i<100;i++){
				rooms.add(new Room("Room"+String.valueOf(i), new Game(), null, null));
			}
		}
		return rooms;
	}

	public Room findRoom(String roomName) {
		for( int i=0; i<rooms.size(); i++ ){
			Room currentRoom = rooms.get(i);
			if( currentRoom.getName().equals(roomName)) {
				return currentRoom;
			}
		}
		return null;
	}
	
}
