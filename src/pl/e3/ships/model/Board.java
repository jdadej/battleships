package pl.e3.ships.model;

import pl.e3.ships.interfaces.IBoard;

public class Board implements IBoard {

	public int[][] states;
	public int winCounter;
	public int counter;
	public int count4;
	public int count31;
	public int count32;
	public int count21;
	public int count22;
	public int count23;
	public int count24;
	public boolean correctShoot;
	
	public Board(){
		setWinCounter(20);
		setCounter(0);
		this.correctShoot=false;
		this.count21=2;this.count22=2;this.count23=2;
		this.count31=3;this.count32=3;
		this.count4=4;
		setStates(new int[10][10]);
		for(int x = 0; x < getStates().length; x++) {
			
			int[] row = getStates()[x];
			
			for(int y = 0; y<row.length; y++){
				getStates()[x][y]=0;
			}
		}		
	}

	public int[][] getStates() {
		return states;
	}
	public void setStates(int[][] states) {
		this.states = states;
	}
	public int getWinCounter(){
		return winCounter;
	}
	public void setWinCounter(int counter){
		this.winCounter=counter;
	}
	public int getCounter(){
		return counter;
	}
	public void setCounter(int counter){
		this.counter=counter;
	}
}
