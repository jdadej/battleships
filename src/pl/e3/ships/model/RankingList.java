package pl.e3.ships.model;

import pl.e3.components.Ranking;
import pl.e3.ships.interfaces.IRankingList;

public class RankingList implements IRankingList {
	public String[] nicks;
	public String[] wins;
	
	public RankingList(){
		setNicks(new String[10]);
		setWins(new String[10]);
		Ranking ranking = new Ranking();
		
		for(int i=0; i<10; i++){
				nicks[i]= (String) ranking.SearchTop100().get(i).getKey();
				wins[i]= (String) String.valueOf(ranking.SearchTop100().get(i).getValue());
		}
	}
	public String[] getNicks() {
		return nicks;
	}
	public void setNicks(String[] nicks) {
		this.nicks = nicks;
	}
	public String[] getWins() {
		return wins;
	}
	public void setWins(String[] wins) {
		this.wins = wins;
	}
}
