package pl.e3.ships.model;

import pl.e3.ships.interfaces.IUser;

public class User implements IUser {
	
	String username;
	String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String user) {
		username = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String pass) {
		password = pass;
	}
}
