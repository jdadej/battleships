package pl.e3.ships.model;

import java.util.Random;

import pl.e3.ships.interfaces.IGame;

public class Game implements IGame{

	public Board player1; //plansza gracza 1
	public Board player2; //plansza gracza 2
	public int whoMoves; // wskaznik "czyj ruch" 1- gracz1, 2 - gracz2
	public String roomName; //nazwa pokoju w grze multiplayer
	public int[][] shipsList1 = new int[10][10]; //lista z umiejscowieniem statkow gracza 1
	public int[][] shipsList2 = new int[10][10]; // ... i odpowiednio 2

	public Game() {
		this.whoMoves = 1;
		setPlayer1(new Board());
		setPlayer2(new Board());
		setWhoMoves(1);
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				this.shipsList1[x][y] = 0;
				this.shipsList2[x][y] = 0;
			}
		}
	}

	public Board getPlayer1() {
		return player1;
	}

	public Board getPlayer2() {
		return player2;
	}

	public void setPlayer1(Board player1) {
		this.player1 = player1;
	}

	public void setPlayer2(Board player2) {
		this.player2 = player2;
	}

	public int getWhoMoves() {
		return whoMoves;
	}

	public void setWhoMoves(int whoMoves) {
		this.whoMoves = whoMoves;
	}
	
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public int[][] getShipsList1() {
		return shipsList1;
	}

	public void setShipsList1(int[][] shipslist1) {
		this.shipsList1 = shipslist1;
	}
// inicjacja planszy AI
	public void initiateAI(int X, int Y, int zliczacz) {
		Random rand = new Random();
		if ((this.player2.counter == 0 || this.player2.counter == 4
				|| this.player2.counter == 7 || this.player2.counter == 10
				|| this.player2.counter == 12 || this.player2.counter == 14
				|| this.player2.counter == 16 || this.player2.counter > 16)
				&& (this.player2.counter < 20)) {
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					if (this.shipsList2[x][y] == 5 && this.player2.counter == 4) {
						this.shipsList2[x][y] = 4;
					} else if (this.shipsList2[x][y] == 5
							&& this.player2.counter == 7) {
						this.shipsList2[x][y] = 31;
					} else if (this.shipsList2[x][y] == 5
							&& this.player2.counter == 10) {
						this.shipsList2[x][y] = 32;
					} else if (this.shipsList2[x][y] == 5
							&& this.player2.counter == 12) {
						this.shipsList2[x][y] = 21;
					} else if (this.shipsList2[x][y] == 5
							&& this.player2.counter == 14) {
						this.shipsList2[x][y] = 22;
					} else if (this.shipsList2[x][y] == 5
							&& this.player2.counter == 16) {
						this.shipsList2[x][y] = 23;
					} else if (this.shipsList2[x][y] == 5
							&& (this.player2.counter > 16)) {
						this.shipsList2[x][y] = 1;
					}
				}
			}
			zliczacz = 0;
			makeFieldRed();
			X = rand.nextInt(10);
			Y = rand.nextInt(10);
			if (this.shipsList2[X][Y] == 0) {
				this.shipsList2[X][Y] = 5;
				this.player2.counter++;
				zliczacz++;
			} else
				initiateAI(0, 0, 0);
		}

		if (this.player2.counter < 16) {
			if (!checker(X, Y)) {
				this.player2.counter = this.player2.counter - zliczacz;
				initiateAI(0, 0, 0);
			}
			int z = rand.nextInt(4);
			if (z == 0) {
				if (X > 0) {
					if (this.shipsList2[X - 1][Y] == 0) {
						this.shipsList2[X - 1][Y] = 5;
						this.player2.counter++;
						zliczacz++;
						initiateAI(X - 1, Y, zliczacz);
					}
				}
			} else if (z == 1) {
				if (Y > 0) {
					if (this.shipsList2[X][Y - 1] == 0) {
						this.shipsList2[X][Y - 1] = 5;
						this.player2.counter++;
						zliczacz++;
						initiateAI(X, Y - 1, zliczacz);
					}
				}
			} else if (z == 2) {
				if (X < 9) {
					if (this.shipsList2[X + 1][Y] == 0) {
						this.shipsList2[X + 1][Y] = 5;
						this.player2.counter++;
						zliczacz++;
						initiateAI(X + 1, Y, zliczacz);
					}
				}
			} else if (z == 3) {
				if (Y < 9) {
					if (this.shipsList2[X][Y + 1] == 0) {
						this.shipsList2[X][Y + 1] = 5;
						this.player2.counter++;
						zliczacz++;
						initiateAI(X, Y + 1, zliczacz);
					}
				}
			}
			initiateAI(X, Y, zliczacz);
		} else if (this.player2.counter < 20 && this.player2.counter >= 16) {
			initiateAI(X, Y, 0);
		}
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (this.shipsList2[x][y] == 6) {
					this.shipsList2[x][y] = 0;
				}
				if (this.shipsList2[x][y] == 5) {
					this.shipsList2[x][y] = 1;
				}
			}
		}
	}

	public Boolean checker(int x, int y) {
		int fatalError = 0;
		if (x > 0) {
			if (this.shipsList2[x - 1][y] != 0) {
				fatalError++;
			}
		}
		if (x < 9) {
			if (this.shipsList2[x + 1][y] != 0) {
				fatalError++;
			}
		}
		if (y > 0) {
			if (this.shipsList2[x][y - 1] != 0) {
				fatalError++;
			}
		}
		if (y < 9) {
			if (this.shipsList2[x][y + 1] != 0) {
				fatalError++;
			}
		}
		if (fatalError == 4) {
			for (int X = 0; X < 10; X++) {
				for (int Y = 0; Y < 10; Y++) {
					if (this.shipsList2[X][Y] == 5) {
						this.shipsList2[X][Y] = 0;
					}
				}
			}
			return false;
		}
		return true;
	}
// sprawia ze pola, na ktore nie ma sensu klikac, sa 'nieklikalne'
	public void makeFieldRed() {
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (this.shipsList2[x][y] != 0 && this.shipsList2[x][y] != 6) {
					if (x > 0) {
						if (this.shipsList2[x - 1][y] == 0) {
							this.shipsList2[x - 1][y] = 6;
						}
					}
					if (x < 9) {
						if (this.shipsList2[x + 1][y] == 0) {
							this.shipsList2[x + 1][y] = 6;
						}
					}
					if (y > 0) {
						if (this.shipsList2[x][y - 1] == 0) {
							this.shipsList2[x][y - 1] = 6;
						}
					}
					if (y < 9) {
						if (this.shipsList2[x][y + 1] == 0) {
							this.shipsList2[x][y + 1] = 6;
						}
					}
					if (x > 0 && y > 0) {
						if (this.shipsList2[x - 1][y - 1] == 0) {
							this.shipsList2[x - 1][y - 1] = 6;
						}
					}
					if (x < 9 && y < 9) {
						if (this.shipsList2[x + 1][y + 1] == 0) {
							this.shipsList2[x + 1][y + 1] = 6;
						}
					}
					if (y > 0 && x < 9) {
						if (this.shipsList2[x + 1][y - 1] == 0) {
							this.shipsList2[x + 1][y - 1] = 6;
						}
					}
					if (y < 9 && x > 0) {
						if (this.shipsList2[x - 1][y + 1] == 0) {
							this.shipsList2[x - 1][y + 1] = 6;
						}
					}
				}
			}
		}
	}
//zaczepia statki gracza1 na planszy
	public void place1(int X, int Y) {

		if (this.player1.counter < 4) {
			this.shipsList1[X][Y] = 4;
		}
		if (4 <= this.player1.counter && this.player1.counter < 7) {
			this.shipsList1[X][Y] = 31;
		}
		if (7 <= this.player1.counter && this.player1.counter < 10) {
			this.shipsList1[X][Y] = 32;
		}
		if (10 <= this.player1.counter && this.player1.counter < 12) {
			this.shipsList1[X][Y] = 21;
		}
		if (12 <= this.player1.counter && this.player1.counter < 14) {
			this.shipsList1[X][Y] = 22;
		}
		if (14 <= this.player1.counter && this.player1.counter < 16) {
			this.shipsList1[X][Y] = 23;
		}
		if (16 <= this.player1.counter && this.player1.counter < 20) {
			this.shipsList1[X][Y] = 1;
		}
		this.player1.states[X][Y] = 2;

		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (this.player1.states[x][y] == 0) {
					this.player1.states[x][y] = 1;
				}
			}
		}
		if (X > 0) {
			if (this.player1.states[X - 1][Y] != 2
					&& this.player1.states[X - 1][Y] != 3) {
				this.player1.states[X - 1][Y] = 0;
			}
		}
		if (X < 9) {
			if (this.player1.states[X + 1][Y] != 2
					&& this.player1.states[X + 1][Y] != 3) {
				this.player1.states[X + 1][Y] = 0;
			}
		}
		if (Y > 0) {
			if (this.player1.states[X][Y - 1] != 2
					&& this.player1.states[X][Y - 1] != 3) {
				this.player1.states[X][Y - 1] = 0;
			}
		}
		if (Y < 9) {
			if (this.player1.states[X][Y + 1] != 2
					&& this.player1.states[X][Y + 1] != 3) {
				this.player1.states[X][Y + 1] = 0;
			}
		}

		this.player1.counter++;
		// dokowanie statku
		if (this.player1.counter == 4 || this.player1.counter == 7
				|| this.player1.counter == 10 || this.player1.counter == 12
				|| this.player1.counter == 14 || this.player1.counter == 16
				|| this.player1.counter > 16) {
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					if (this.player1.states[x][y] == 2) {
						if (x > 0) {
							if (this.player1.states[x - 1][y] != 2) {
								this.player1.states[x - 1][y] = 3;
							}
						}
						if (x < 9) {
							if (this.player1.states[x + 1][y] != 2) {
								this.player1.states[x + 1][y] = 3;
							}
						}
						if (y > 0) {
							if (this.player1.states[x][y - 1] != 2) {
								this.player1.states[x][y - 1] = 3;
							}
						}
						if (y < 9) {
							if (this.player1.states[x][y + 1] != 2) {
								this.player1.states[x][y + 1] = 3;
							}
						}
						if (x > 0 && y > 0) {
							if (this.player1.states[x - 1][y - 1] != 2) {
								this.player1.states[x - 1][y - 1] = 3;
							}
						}
						if (x < 9 && y < 9) {
							if (this.player1.states[x + 1][y + 1] != 2) {
								this.player1.states[x + 1][y + 1] = 3;
							}
						}
						if (y > 0 && x < 9) {
							if (this.player1.states[x + 1][y - 1] != 2) {
								this.player1.states[x + 1][y - 1] = 3;
							}
						}
						if (y < 9 && x > 0) {
							if (this.player1.states[x - 1][y + 1] != 2) {
								this.player1.states[x - 1][y + 1] = 3;
							}
						}
					}
				}
			}
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					if (this.player1.states[x][y] != 2
							&& this.player1.states[x][y] != 3) {
						this.player1.states[x][y] = 0;
					}
				}
			}
		}
	}
//zaczepia statki gracza2 na planszy (umiejscawia)
	public void place2(int X, int Y) {

		if (this.player2.counter < 4) {
			this.shipsList2[X][Y] = 4;
		}
		if (4 <= this.player2.counter && this.player2.counter < 7) {
			this.shipsList2[X][Y] = 31;
		}
		if (7 <= this.player2.counter && this.player2.counter < 10) {
			this.shipsList2[X][Y] = 32;
		}
		if (10 <= this.player2.counter && this.player2.counter < 12) {
			this.shipsList2[X][Y] = 21;
		}
		if (12 <= this.player2.counter && this.player2.counter < 14) {
			this.shipsList2[X][Y] = 22;
		}
		if (14 <= this.player2.counter && this.player2.counter < 16) {
			this.shipsList2[X][Y] = 23;
		}
		if (16 <= this.player2.counter && this.player2.counter < 20) {
			this.shipsList2[X][Y] = 1;
		}
		this.player2.states[X][Y] = 2;

		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (this.player2.states[x][y] == 0) {
					this.player2.states[x][y] = 1;
				}
			}
		}
		if (X > 0) {
			if (this.player2.states[X - 1][Y] != 2
					&& this.player2.states[X - 1][Y] != 3) {
				this.player2.states[X - 1][Y] = 0;
			}
		}
		if (X < 9) {
			if (this.player2.states[X + 1][Y] != 2
					&& this.player2.states[X + 1][Y] != 3) {
				this.player2.states[X + 1][Y] = 0;
			}
		}
		if (Y > 0) {
			if (this.player2.states[X][Y - 1] != 2
					&& this.player2.states[X][Y - 1] != 3) {
				this.player2.states[X][Y - 1] = 0;
			}
		}
		if (Y < 9) {
			if (this.player2.states[X][Y + 1] != 2
					&& this.player2.states[X][Y + 1] != 3) {
				this.player2.states[X][Y + 1] = 0;
			}
		}

		this.player2.counter++;
		// dokowanie statku
		if (this.player2.counter == 4 || this.player2.counter == 7
				|| this.player2.counter == 10 || this.player2.counter == 12
				|| this.player2.counter == 14 || this.player2.counter == 16
				|| this.player2.counter > 16) {
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					if (this.player2.states[x][y] == 2) {
						if (x > 0) {
							if (this.player2.states[x - 1][y] != 2) {
								this.player2.states[x - 1][y] = 3;
							}
						}
						if (x < 9) {
							if (this.player2.states[x + 1][y] != 2) {
								this.player2.states[x + 1][y] = 3;
							}
						}
						if (y > 0) {
							if (this.player2.states[x][y - 1] != 2) {
								this.player2.states[x][y - 1] = 3;
							}
						}
						if (y < 9) {
							if (this.player2.states[x][y + 1] != 2) {
								this.player2.states[x][y + 1] = 3;
							}
						}
						if (x > 0 && y > 0) {
							if (this.player2.states[x - 1][y - 1] != 2) {
								this.player2.states[x - 1][y - 1] = 3;
							}
						}
						if (x < 9 && y < 9) {
							if (this.player2.states[x + 1][y + 1] != 2) {
								this.player2.states[x + 1][y + 1] = 3;
							}
						}
						if (y > 0 && x < 9) {
							if (this.player2.states[x + 1][y - 1] != 2) {
								this.player2.states[x + 1][y - 1] = 3;
							}
						}
						if (y < 9 && x > 0) {
							if (this.player2.states[x - 1][y + 1] != 2) {
								this.player2.states[x - 1][y + 1] = 3;
							}
						}
					}
				}
			}
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					if (this.player2.states[x][y] != 2
							&& this.player2.states[x][y] != 3) {
						this.player2.states[x][y] = 0;
					}
				}
			}
		}
	}
//analiza ruchu gracza1
	public int player1Move(int r, int c) {
		this.player1.correctShoot=false;
		if (this.shipsList2[r][c] == 0) {
			return this.player2.states[r][c] = 1;
		} else if (this.shipsList2[r][c] == 4 || this.shipsList2[r][c] == 31
				|| this.shipsList2[r][c] == 32 || this.shipsList2[r][c] == 21
				|| this.shipsList2[r][c] == 22 || this.shipsList2[r][c] == 23) {
			if (this.shipsList2[r][c] == 4) {
				this.player1.count4--;
			}
			if (this.shipsList2[r][c] == 31) {
				this.player1.count31--;
			}
			if (this.shipsList2[r][c] == 32) {
				this.player1.count32--;
			}
			if (this.shipsList2[r][c] == 21) {
				this.player1.count21--;
			}
			if (this.shipsList2[r][c] == 22) {
				this.player1.count22--;
			}
			if (this.shipsList2[r][c] == 23) {
				this.player1.count23--;
			}
			this.player1.winCounter--;
			this.player1.correctShoot=true;
			return this.player2.states[r][c] = 2;
		} else {
			this.player1.winCounter--;
			this.player1.correctShoot=true;
			return this.player2.states[r][c] = 2;
		}
	}
//analiza ruchu gracza2
	public int player2Move(int r, int c) {
		this.player2.correctShoot=false;
		if (this.shipsList1[r][c] == 0) {
			return this.player1.states[r][c] = 1;
		} else if (this.shipsList1[r][c] == 4 || this.shipsList1[r][c] == 31
				|| this.shipsList1[r][c] == 32 || this.shipsList1[r][c] == 21
				|| this.shipsList1[r][c] == 22 || this.shipsList1[r][c] == 23) {
			if (this.shipsList1[r][c] == 4) {
				this.player2.count4--;
			}
			if (this.shipsList1[r][c] == 31) {
				this.player2.count31--;
			}
			if (this.shipsList1[r][c] == 32) {
				this.player2.count32--;
			}
			if (this.shipsList1[r][c] == 21) {
				this.player2.count21--;
			}
			if (this.shipsList1[r][c] == 22) {
				this.player2.count22--;
			}
			if (this.shipsList1[r][c] == 23) {
				this.player2.count23--;
			}
			this.player2.winCounter--;
			this.player2.correctShoot=true;
			return this.player1.states[r][c] = 2;
		} else {
			this.player2.winCounter--;
			this.player2.correctShoot=true;
			return this.player1.states[r][c] = 2;
		}
	}
//sprawdzanie poprawnosci planszy gracza1
	public void player1Checker() {

		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (this.player1.count4 == 0 && this.shipsList2[x][y] == 4) {
					this.player2.states[x][y] = 3;
				}
				if (this.player1.count31 == 0 && this.shipsList2[x][y] == 31) {
					this.player2.states[x][y] = 3;
				}
				if (this.player1.count32 == 0 && this.shipsList2[x][y] == 32) {
					this.player2.states[x][y] = 3;
				}
				if (this.player1.count21 == 0 && this.shipsList2[x][y] == 21) {
					this.player2.states[x][y] = 3;
				}
				if (this.player1.count22 == 0 && this.shipsList2[x][y] == 22) {
					this.player2.states[x][y] = 3;
				}
				if (this.player1.count23 == 0 && this.shipsList2[x][y] == 23) {
					this.player2.states[x][y] = 3;
				}
			}
		}
	}
//sprawdzanie poprawnosci planszy gracza2
	public void player2Checker() {

		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (this.player2.count4 == 0 && this.shipsList1[x][y] == 4) {
					this.player1.states[x][y] = 3;
				}
				if (this.player2.count31 == 0 && this.shipsList1[x][y] == 31) {
					this.player1.states[x][y] = 3;
				}
				if (this.player2.count32 == 0 && this.shipsList1[x][y] == 32) {
					this.player1.states[x][y] = 3;
				}
				if (this.player2.count21 == 0 && this.shipsList1[x][y] == 21) {
					this.player1.states[x][y] = 3;
				}
				if (this.player2.count22 == 0 && this.shipsList1[x][y] == 22) {
					this.player1.states[x][y] = 3;
				}
				if (this.player2.count23 == 0 && this.shipsList1[x][y] == 23) {
					this.player1.states[x][y] = 3;
				}
			}
		}
	}
//wykonanie ruchu przez AI
	public int AI() {

		Random rand = new Random();
		int X = rand.nextInt(10);
		int Y = rand.nextInt(10);

		if (this.shipsList1[X][Y] == 0) {
			this.shipsList1[X][Y] = 5;
			return this.player1.states[X][Y] = 1;
		} else if (this.shipsList1[X][Y] == 5 || this.shipsList1[X][Y] == 104
				|| this.shipsList1[X][Y] == 131 || this.shipsList1[X][Y] == 132
				|| this.shipsList1[X][Y] == 121 || this.shipsList1[X][Y] == 122
				|| this.shipsList1[X][Y] == 123 || this.shipsList1[X][Y] == 101) {
			return AI();
		} else {
			player2Move(X, Y);
			this.shipsList1[X][Y] = this.shipsList1[X][Y] + 100;
			AIchecker();
			this.player1.states[X][Y] = 2;
			return AI();
		}
	}
//sprawdzenie poprawnosci planszy AI
	public void AIchecker() {

		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (this.player2.count4 == 0 && this.shipsList1[x][y] == 104) {
					this.player1.states[x][y] = 3;
					makeRedAI(104);
				}
				if (this.player2.count31 == 0 && this.shipsList1[x][y] == 131) {
					this.player1.states[x][y] = 3;
					makeRedAI(131);
				}
				if (this.player2.count32 == 0 && this.shipsList1[x][y] == 132) {
					this.player1.states[x][y] = 3;
					makeRedAI(132);
				}
				if (this.player2.count21 == 0 && this.shipsList1[x][y] == 121) {
					this.player1.states[x][y] = 3;
					makeRedAI(121);
				}
				if (this.player2.count22 == 0 && this.shipsList1[x][y] == 122) {
					this.player1.states[x][y] = 3;
					makeRedAI(122);
				}
				if (this.player2.count23 == 0 && this.shipsList1[x][y] == 123) {
					this.player1.states[x][y] = 3;
					makeRedAI(123);
				}
				if (this.shipsList1[x][y] == 101) {
					this.player1.states[x][y] = 3;
					makeRedAI(101);
				}
			}
		}
	}
//sprawdzenie dla AI w ktore pola po zatopieniu statku nie warto strzelac. 
	public void makeRedAI(int sink) {
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (this.shipsList1[x][y] == sink) {
					if (x > 0) {
						if (this.shipsList1[x - 1][y] == 0) {
							this.shipsList1[x - 1][y] = 5;
							this.player1.states[x - 1][y]=1;
						}
					}
					if (x < 9) {
						if (this.shipsList1[x + 1][y] == 0) {
							this.shipsList1[x + 1][y] = 5;
							this.player1.states[x + 1][y]=1;
						}
					}
					if (y > 0) {
						if (this.shipsList1[x][y - 1] == 0) {
							this.shipsList1[x][y - 1] = 5;
							this.player1.states[x][y - 1]=1;
						}
					}
					if (y < 9) {
						if (this.shipsList1[x][y + 1] == 0) {
							this.shipsList1[x][y + 1] = 5;
							this.player1.states[x][y + 1]=1;
						}
					}
					if (x > 0 && y > 0) {
						if (this.shipsList1[x - 1][y - 1] == 0) {
							this.shipsList1[x - 1][y - 1] = 5;
							this.player1.states[x - 1][y - 1]=1;
						}
					}
					if (x < 9 && y < 9) {
						if (this.shipsList1[x + 1][y + 1] == 0) {
							this.shipsList1[x + 1][y + 1] = 5;
							this.player1.states[x + 1][y + 1]=1;
						}
					}
					if (y > 0 && x < 9) {
						if (this.shipsList1[x + 1][y - 1] == 0) {
							this.shipsList1[x + 1][y - 1] = 5;
							this.player1.states[x + 1][y - 1]=1;
						}
					}
					if (y < 9 && x > 0) {
						if (this.shipsList1[x - 1][y + 1] == 0) {
							this.shipsList1[x - 1][y + 1] = 5;
							this.player1.states[x - 1][y + 1]=1;
						}
					}
				}
			}
		}
	}
//sprawdzenie dla gracza1 w ktore pola po zatopieniu statku nie warto strzelac
	public void makeRed1() {
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (this.player1.states[x][y] == 3) {
					if (x > 0) {
						if (this.shipsList1[x - 1][y] != 3) {
							this.player1.states[x - 1][y] = 1;
						}
					}
					if (x < 9) {
						if (this.player1.states[x + 1][y] != 3) {
							this.player1.states[x + 1][y] = 1;
						}
					}
					if (y > 0) {
						if (this.player1.states[x][y - 1] != 3) {
							this.player1.states[x][y - 1] = 1;
						}
					}
					if (y < 9) {
						if (this.player1.states[x][y + 1] != 3) {
							this.player1.states[x][y + 1] = 1;
						}
					}
					if (x > 0 && y > 0) {
						if (this.player1.states[x - 1][y - 1] != 3) {
							this.player1.states[x - 1][y - 1] = 1;
						}
					}
					if (x < 9 && y < 9) {
						if (this.player1.states[x + 1][y + 1] != 3) {
							this.player1.states[x + 1][y + 1] = 1;
						}
					}
					if (y > 0 && x < 9) {
						if (this.player1.states[x + 1][y - 1] != 3) {
							this.player1.states[x + 1][y - 1] = 1;
						}
					}
					if (y < 9 && x > 0) {
						if (this.player1.states[x - 1][y + 1] != 3) {
							this.player1.states[x - 1][y + 1] = 1;
						}
					}
				}
			}
		}
	}
//sprawdznie dla gracza2 w ktore pola po zatopieniu statku nie warto strzelac
	public void makeRed2() {
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (this.player2.states[x][y] == 3) {
					if (x > 0) {
						if (this.player2.states[x - 1][y] != 3) {
							this.player2.states[x - 1][y] = 1;
						}
					}
					if (x < 9) {
						if (this.player2.states[x + 1][y] != 3) {
							this.player2.states[x + 1][y] = 1;
						}
					}
					if (y > 0) {
						if (this.player2.states[x][y - 1] != 3) {
							this.player2.states[x][y - 1] = 1;
						}
					}
					if (y < 9) {
						if (this.player2.states[x][y + 1] != 3) {
							this.player2.states[x][y + 1] = 1;
						}
					}
					if (x > 0 && y > 0) {
						if (this.player2.states[x - 1][y - 1] != 3) {
							this.player2.states[x - 1][y - 1] = 1;
						}
					}
					if (x < 9 && y < 9) {
						if (this.player2.states[x + 1][y + 1] != 3) {
							this.player2.states[x + 1][y + 1] = 1;
						}
					}
					if (y > 0 && x < 9) {
						if (this.player2.states[x + 1][y - 1] != 3) {
							this.player2.states[x + 1][y - 1] = 1;
						}
					}
					if (y < 9 && x > 0) {
						if (this.player2.states[x - 1][y + 1] != 3) {
							this.player2.states[x - 1][y + 1] = 1;
						}
					}
				}
			}
		}
	}
}
