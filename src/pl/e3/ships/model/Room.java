package pl.e3.ships.model;

import pl.e3.ships.interfaces.IRoom;

public class Room implements IRoom {
	
	public String name;
	public Game game;
	public String login1;
	public String login2;
	
	public Room(String name, Game game, String login1, String login2) {
		super();
		this.name = name;
		this.game = game;
		this.login1 = login1;
		this.login2 = login2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getLogin1() {
		return login1;
	}

	public void setLogin1(String login1) {
		this.login1 = login1;
	}

	public String getLogin2() {
		return login2;
	}

	public void setLogin2(String login2) {
		this.login2 = login2;
	}
	
	

}
