package pl.e3.ships.model;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import pl.e3.ships.interfaces.IUserDAO;
import pl.e3.ships.model.User;

public class UserDAO implements IUserDAO {
	//private DataSource dataSource;
	private static JdbcTemplate jdbcTemplateObject;
	 
	public void setDataSource(DataSource ds) {
	    jdbcTemplateObject = new JdbcTemplate(ds);
	}
	public static List<User> listStudents() {
			String SQL = "select * from classicmodels.users";
		      //User user = new User();
		      //user.setUsername("test");
		      //user.setPassword("test");
		      List <User> students = jdbcTemplateObject.query(SQL, 
		                                new UserMapper());
		      //List<User> students = new ArrayList<User>();
		      //students.add(user);
		      return students;
	}
	public void createNewUser(String log, String pas){
		String sql = "INSERT INTO classicmodels.users (username,password) VALUES('"+log+"','"+pas+"');";
		jdbcTemplateObject.execute(sql);
		String sql1 = "INSERT INTO classicmodels.user_roles (username,role) VALUES('"+log+"','ROLE_USER');";
		jdbcTemplateObject.execute(sql1);
	}
}
